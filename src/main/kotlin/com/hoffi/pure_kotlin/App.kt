package com.hoffi.pure_kotlin

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.default
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import java.util.*

/**
 * also have a look at the kotlin interactive tutorial playground: https://play.kotlinlang.org/byExample/overview
 */
fun main(args: Array<String>) {
    App().main(args)
}

/**
 * Extension Function
 * with the receiver "String."
 * the class String will now have a method: titlecase()
 * the actual receiver Instance of String is accessible with "this"
 * beware: only public things can be called by Extension functions no acces to internal/private receiver class stuff
 */
fun String.titlecase() = this.replaceFirstChar { it.titlecase(Locale.getDefault()) }

class App : CliktCommand() { // runtime args and opts parsing with multiplatform library Clikt (Cli for kt = kotlin)
    private val cmd by argument(help = "cmd to execute").default("hello")
    private val name by option(help = "your name").default(System.getenv("USER"))

    override fun run() { // Clikt entry point
        when (cmd) { // when is the _better_ if/then/else/switch
            "hello" -> hello()
            "filter" -> filter()
        }
    }

    private fun hello() {
        println("Hello ${name.titlecase()}!")
    }

    /**
     * test("filter") {
     *   App().filter() shouldBe "'10', '20'"
     * }
     */
    fun filter(): String {
        val numbers = 1..10
        // one time parsing over the Int Range until a terminating function is met
        // you can eve let the iteration run in parallel without changing the filter { } and map { } call
        val s = numbers.filter { it % 5 == 0 }
            .map { it * 2}
            .joinToString("', '", "'", "'")
        return s
    }
    fun filter2(numbers: List<Int>): String {
        val s = numbers.filter { it % 5 == 0 }
            .map { it * 2}
            .joinToString("', '", "'", "'")
        return s
    }
}
