package com.hoffi.pure_kotlin

object ClosureWithState {
    fun closureMaker(): () -> Int {
        var state = 0
        return { ++state }
    }

    fun doIt(): List<Int> {
        val counter1 = closureMaker()
        val counter2 = closureMaker()

        val result = mutableListOf<Int>()
        result.add( counter1() ) // 1
        result.add( counter1() ) // 2
        result.add( counter1() ) // 3
        result.add( counter1() ) // 4
        result.add( counter2() ) // 1
        result.add( counter2() ) // 2
        result.add( counter1() ) // 5
        return result
    }
}
