package com.hoffi.pure_kotlin

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class AppTest : FunSpec({

    test("default") {
        App().main(emptyList())
    }

    test("filter") {
        App().filter() shouldBe "'10', '20'"
    }
})

class AppBDDTest : BehaviorSpec({
    given("the app and numbers 1 to 10") {
        val app = App()
        val numbers = (1..10).toList()

        When("numbers are filtered") {
            then("the filtered result should be '10', '20'") {
                app.filter2(numbers) shouldBe "'10', '20'"
            }
        }
        When("no numbers are given") {
            then("the filtered result should be empty") {
                app.filter2(emptyList()) shouldBe "''"
            }
        }
    }
})
