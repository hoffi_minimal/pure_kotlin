package com.hoffi.pure_kotlin

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe

class ClosureWithStateTest : BehaviorSpec({

    given("two statefull closureMaker lambdas") {
        and("a sequence of closure calls") {
            then("the output list should be") {
                ClosureWithState.doIt() shouldContainExactly listOf(1, 2, 3, 4, 1, 2, 5)
            }
        }
    }
})
